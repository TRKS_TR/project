import java.util.List;
import java.util.Scanner;

public class Library {
    private List<Book> books;

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
    public void addBook(Book bk){
        this.books.add(bk);
    }
    public void deleteBook(Book bk){
        this.books.remove(bk);
    }
    public Book searchBook(String name, int type) {
        Scanner scan = new Scanner(System.in);
        Book b=null;
        switch (type) {
            case 1:
                for (Book a : books) {
                    if (a.getName() == name) {
                        System.out.println("Была найдена следующая книга\n"+a.toString()+"\nЕсли эта книга, которую искали, введите 1");
                        String k = scan.next();
                        if(k.equals("1")){
                            return a;
                        }else{
                            continue;
                        }
                    }
                }
                System.out.println("Книга не найдена");
                break;
            case 2:
                for (Book a : books) {
                    if (a.getAuthor() == name) {
                        System.out.println("Была найдена следующая книга\n"+a.toString()+"\nЕсли эта книга, которую искали, введите 1");
                        String k = scan.next();
                        if(k.equals("1")){
                            return a;
                        }else{
                            continue;
                        }
                    }
                }
                System.out.println("Книга не найдена");
                break;
        }
        return b;
    }
    public Book searchBook(int year){
        Scanner scan = new Scanner(System.in);
        Book b = null;
        for(Book a: books){
            if(a.getYear()==year){
                System.out.println("Была найдена следующая книга\n"+a.toString()+"\nЕсли эта книга, которую искали, введите 1");
                String k = scan.next();
                if(k.equals("1")){
                    return a;
                }else{
                    continue;
                }
            }
        }
        System.out.println("Книга не найдена");
        return b;
    }
    public Library(List<Book> books){
        this.books = books;
    }
}
